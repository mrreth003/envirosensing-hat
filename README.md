EEE3088F Envirosensing HAT PCB design group project for EEE3088F
 
Design Team: 
Ethan Morris MRRETH003
Ethan Meknassi MKNETH002
Moutloatsi Setlogelo STLMOU001
 
The concept proposal that the group came up with is based on a proximity sensor surveillance device The HAT, powered by the Microcontroller or by a battery, would detect the presence of a person/animal using a PIR motion sensor. This motion will trigger a buzzer to activate and make a noise. The log of the different detections is gathered and can be accessed by the user.
 
The required hardware the user would need for this device to be operational are as follows:
- An STM32000 Microcontroller or an 18650 battery to power the device
- Male USB A to Male Micro USB B Cable to get information gathered from the device
- A computer to retrieve the data from the device
 
The connection of the hardware can vary depending on whether the device is being powered up by a microcontroller or by the battery. For the Microcontroller setup, the microcontroller must be placed under the device through the inner set of connectors. The 5V pin of the microcontroller must be connected to pin "J3" on the device (circled in green in the picture attached). This is the pin that is the furthest to the left on the third row of connectors when looking at the device in its default position (buzzer to the top left). 
For the battery setup, the battery is placed in the battery holder and will power the device. The USB to micro-USB cable is connected to the device and then to a computer when the information wants to be gathered or the battery to be charged. The position of the battery needs to place in the red rectangle shown in the attached picture.
 
The basic operating mode of the device can be achieved by turning on the device (powering it with the battery) and then creating movement very close to the device (waving hand 5-10cm away from the device). This should create a beeping sound from the buzzer which would mean the device is operational. The log can now be gathered from the device by connecting the device to a computer with the USB to Micro USB cable.

The license of the device can be found in the GIT repository.  
https://gitlab.com/mrreth003/envirosensing-hat/-/tree/main/ 
